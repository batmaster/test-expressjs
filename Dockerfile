FROM node:lts
WORKDIR /app
ENV NODE_ENV production
ARG COMMIT_ID
ENV COMMIT_ID=${COMMIT_ID}
COPY . /app
RUN npm ci --only=production
EXPOSE 3000
CMD ["npm", "start"]
