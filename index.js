const compression = require('compression')
const express = require('express');
const app = express()

const port = 3000;

app.use(compression())

app.get('/', (req, res) => {
    res.send({
      status: 'ok',
      version: process.env.COMMIT_ID
    });
});

const server = app.listen(port, () => {
   const {address, port} = server.address()
    console.log(`Listening at http://${address}:${port}`);
});
